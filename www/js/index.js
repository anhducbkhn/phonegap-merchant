var url = "http://128.199.184.145/merchant_pg";
var domain_name = "http://128.199.184.145";
 
var deviceToken;
var iframe;
var userAuthToken = "";

var app = {
    initialize: function () {
        this.bindEvents();
    },
    bindEvents: function () {
        document.addEventListener("deviceready", function (id) {
            console.log('Received Event: ' + id);

            // register device to GCM
            window.plugins.GCM.register("659096344102", "app.GCMEvent", app.GCMSuccess, app.GCMError);

            iframe = document.getElementById('iframe');

            // register
            deviceToken = window.localStorage.getItem("apptoken");
            console.log('Device Token : ' + deviceToken);
            if (deviceToken == "" || deviceToken == "undefined" || deviceToken == 'null') {
                RegisterDevice();
            }                	

            userAuthToken = window.localStorage.getItem("AuthToken");
            if (userAuthToken == "") {
                
            } else {
                iframe.contentWindow.postMessage({
                    devicetoken: deviceToken
                }, "*");

                iframe.contentWindow.postMessage({
                    loggedin: "true",
                    AuthToken: userAuthToken
                }, "*");

            }

            // Announce that we have a camera.
            iframe.addEventListener("load", function (event) {
                iframe.contentWindow.postMessage({
                    cameraEnabled: navigator.camera != null
                }, "*");
            }, false);

            window.addEventListener("message", function (event) {
                if (event.origin == domain_name) {
                    console.log(event.data);
                    if (event.data == "camera") {
                        cordova.plugins.barcodeScanner.scan(
                                function (result) {
                                    var scan_output = "Barcode value: " + result.text + " and Format: " + result.format;
                                    console.log(scan_output);

                                    app.updateBarcode(scan_output);
                                },
                                function (error) {
                                    iframe.contentWindow.postMessage({
                                        error: message
                                    }, "*");
                                }
                        );
                    }

                    if (event.data.substring(0, 5) == "login") {
                        var userInfo = JSON.parse(event.data.substring(5, event.data.length));
                        console.log(JSON.stringify(userInfo));

                        var url = "http://128.199.184.145/api/sessions";
                        $.ajax(
                                {
                                    type: "POST",
                                    cache: false,
                                    crossDomain: true,
                                    url: url,
                                    headers: {'appToken': deviceToken},
                                    dataType: 'json',
                                    data: {'email': userInfo.email, 'password': userInfo.password, 'type': userInfo.type},
                                    success: function (success) {
                                        console.log('login api response : ' + JSON.stringify(success));
                                        var userInfo = success.user;
                                        userAuthToken = userInfo.X_auth_token;
                                        window.localStorage.setItem("AuthToken", userAuthToken);

                                        iframe.contentWindow.postMessage({
                                            login: userAuthToken
                                        }, "*");
                                    },
                                    error: function (error) {
                                        iframe.contentWindow.postMessage({
                                            loggedin: error
                                        }, "*");
                                    }
                                }
                        );
                    }

                    if (event.data == "logout") {
                        var url = "http://128.199.184.145/api/accounts/logout";
                        $.ajax(
                                {
                                    type: "POST",
                                    cache: false,
                                    crossDomain: true,
                                    url: url,
                                    headers: {'appToken': deviceToken, 'X-auth-token': userAuthToken},
                                    dataType: 'json',
                                    data: {},
                                    success: function (success) {
                                        console.log('Logout Result : ' + JSON.stringify(success));
                                        window.localStorage.setItem("AuthToken", "");
                                        iframe.contentWindow.postMessage({
                                            logout: true
                                        }, "*");
                                    },
                                    error: function (error) {
                                        iframe.contentWindow.postMessage({
                                            logout: false
                                        }, "*");
                                    }
                                }
                        );
                    }
                }
            }, false);
            iframe.src = url;
        }, false);

    },
    GCMEvent: function (e) {
        switch (e.event)
        {
            case 'registered':
                {
                    console.log('GCM Registration Success: device token : ' + e.regid);
                    var register_url = "http://128.199.184.145/gcm/registerGCMdevice.php?task=register&deviceuid=" + e.regid + "&devicename=" + device.model + "&deviceemail=";

                    console.log(register_url);

                    $.ajax({
                        type: "POST",
                        url: register_url,
                        cache: !1,
                        data: "",
                        timeout: 1E3,
                        success: function (c) {
                            console.log('Successfully subscribed to your server' + c);
                            alert('You successfully subscribed to GCM.');
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('Subscription error : ' + xhr.status + " / " + thrownError);
                        },
                        complete: function () {
                        }
                    })
                }
            case 'message':
            {
                console.log('GCM message: ' + e.message);
                iframe.contentWindow.postMessage({
                    notification: e.message
                }, "*");
            }
        }

    },
    GCMSuccess: function (success) {
        console.log('GCM Registration success ! ');
    },
    GCMError: function (error) {
        console.log('GCM Registration error: ' + JSON.stringify(error));
    },
    updateBarcode: function (result) {
        iframe.contentWindow.postMessage({
            scan: result
        }, "*");

    },
    onDeviceReady: function () {
        app.receivedEvent('deviceready');
    },
    receivedEvent: function (id) {
        console.log('Device Ready fired');
    }
};

function RegisterDevice() {
    $.ajax({
        url: "http://128.199.184.145/api/devices",
        crossDomain: true,
        type: 'POST',
        dataType: 'json',
        data: {'username': 'shampoo', 'password': '123456', 'device_id': 'ABCD112112'},
        success: function (data) {
            //console.log('Response : ' + JSON.stringify(data));

            window.localStorage.setItem("apptoken", data.appToken);
            deviceToken = window.localStorage.getItem("apptoken");

            iframe.contentWindow.postMessage({
                devicetoken: deviceToken
            }, "*");

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert('A error has occurred. Status: ' + textStatus + ' - Message: ' + errorThrown);
        }
    })

}
app.initialize();